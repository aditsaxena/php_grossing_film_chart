<?php
  
  if (!defined('PHP_ENV')) define('PHP_ENV', 'development');
  
  if (!defined('ROOT_PATH')) {
    define('ROOT_PATH', getcwd());

    switch (PHP_ENV) {
      case 'test':
      case 'development':
        define('XML_URI', ROOT_PATH.'/static_support/wikipedia_highest_grossing_films.xml');
        break;
      
      case 'production':
        define('XML_URI', 'http://en.wikipedia.org/wiki/List_of_highest-grossing_films');
        break;

    }

    define('OUTPUT_IMAGE', ROOT_PATH.'/output.png');
  }
?>