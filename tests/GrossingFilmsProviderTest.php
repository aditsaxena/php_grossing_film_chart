<?php
  require_once 'tests/testHelper.php';

  class GrossingFilmsProviderTest extends PHPUnit_Framework_TestCase
  {
    public function __construct()
    {
      $string_xml = file_get_contents(XML_URI);
      $this->grossing_films_class = new GrossingFilmsProvider($string_xml);
    }

    public function testExtractBasic()
    {
      $grossing_films = $this->grossing_films_class->extract();

      $this->assertEquals(count($grossing_films), 50);
    }

    public function testExtractData()
    {
      $grossing_films = $this->grossing_films_class->extract();

      $first_element = $grossing_films[0];
      $this->assertEquals($first_element['gross'], 2782275172);
      $this->assertEquals($first_element['year'], 2009);
      $this->assertEquals($first_element['title'], 'Avatar');

      $last_element = $grossing_films[count($grossing_films) - 1];
      $this->assertEquals($last_element['gross'], 769679473);
      $this->assertEquals($last_element['year'], 2009);
      $this->assertEquals($last_element['title'], '2012');
    }

    public function testLoadsXmlFromURI()
    {
      $this->grossing_films_class = new GrossingFilmsProvider();
      $this->grossing_films_class->loadFromURI(XML_URI);
      $grossing_films = $this->grossing_films_class->extract();

      $this->assertEquals(get_class($this->grossing_films_class->xml), 'SimpleXMLElement');
      $this->assertEquals(count($grossing_films), 50);
    }

    public function testReportsOnEmptyInput()
    {
      $this->setExpectedException('XmlNotLoaded');

      $this->grossing_films_class = new GrossingFilmsProvider();
      $grossing_films = $this->grossing_films_class->extract();
      
    }

    public function testReportsOnInvalidInput()
    {
      $this->setExpectedException('Exception');

      $this->grossing_films_class = new GrossingFilmsProvider("<some><invalid></xml></some>");
      $grossing_films = $this->grossing_films_class->extract();
    }


    public function testReportsEmptyDataOnEmptyInput()
    {
      $this->grossing_films_class = new GrossingFilmsProvider("<some></some>");
      $grossing_films = $this->grossing_films_class->extract();

      $this->assertEquals(count($grossing_films), 0);
    }

  }
