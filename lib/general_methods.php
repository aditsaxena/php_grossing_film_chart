<?php
  
  function array_pluck($key, $array)
  {
      if (is_array($key) || !is_array($array)) return array();
      $funct = create_function('$e', 'return is_array($e) && array_key_exists("'.$key.'",$e) ? $e["'. $key .'"] : null;');
      return array_map($funct, $array);
  }
