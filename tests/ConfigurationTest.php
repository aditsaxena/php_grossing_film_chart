<?php

  define('PHP_ENV', 'test');
  require_once 'config.php';

  class ConfigurationTest extends PHPUnit_Framework_TestCase
  {
    public function testConfiguration()
    {
      $this->assertEquals(XML_URI, ROOT_PATH.'/static_support/wikipedia_highest_grossing_films.xml');
    }

  }
