<?php
  define('PHP_ENV', isset($_GET['debug']) ? 'development' : 'production');

  error_reporting(E_ALL);

  header('Content-Type: text/html; charset=utf-8');

  include 'config.php';
  include 'lib/GrossingFilmsProvider.php';
  include 'lib/GrossingFilmsChart.php';

  $grossing_films_class = new GrossingFilmsProvider();
  
  $grossing_films_class->loadFromURI(XML_URI);
  $grossing_films = $grossing_films_class->extract();
  
  // echo '<pre>'.print_r($grossing_films, true).'</pre>'; exit();

  $grossing_films_chart = new GrossingFilmsChart($grossing_films, [
      'show_best_fit' => (isset($_GET['show_best_fit']) ? $_GET['show_best_fit'] : false),
              'width' => (isset($_GET['width']) ? $_GET['width'] : 0),
             'height' => (isset($_GET['height']) ? $_GET['height'] : 0),
         'background' => (isset($_GET['background']) ? explode(',',$_GET['background']) : null),
  ]);

  $grossing_films_chart->draw();

