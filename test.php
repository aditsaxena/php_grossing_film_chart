<html>
<head>
  <title>Grossing Films Chart</title>
  <style type="text/css">
    html { background: #549499; text-align: center; font-family: 'Helvetica' }
    h2 { margin: 10px 0 5px 0; color: #14163C; }
    p { margin: 0 0 30px 0; }
    p img { border: solid 2px #549499; }
  </style>
</head>
<body>
  <h2>Default Grossing Films Chart (live)</h2>
  <p><img src="chart_image_provider.php"></p>
  <h2>Same chart with different parameters (offline)</h2>
  <p><img src="chart_image_provider.php?debug=true&amp;width=800&amp;height=200&amp;show_best_fit=true&amp;background=255,247,176"></p>
</body>
</html>
