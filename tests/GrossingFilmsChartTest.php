<?php
  require_once 'tests/testHelper.php';

  class GrossingFilmsChartTest extends PHPUnit_Framework_TestCase
  {
    public function __construct()
    {
      include 'static_support/array_grossing_films.php';
      
      $this->GROSSING_FILMS = $ARRAY_GROSSING_FILMS;
    }

    public function testEmptySetOptions()
    {
      $grossing_films_charts = [
        new GrossingFilmsChart($this->GROSSING_FILMS),
        new GrossingFilmsChart($this->GROSSING_FILMS, [
            'show_best_fit' => false,
                    'width' => 0,
                   'height' => 0,
               'background' => null,
        ]),
      ];

      foreach ($grossing_films_charts as $i => $grossing_films_chart) {
        $this->assertEquals($grossing_films_chart->options['show_best_fit'], false);
        $this->assertEquals($grossing_films_chart->options['width'], 300);
        $this->assertEquals($grossing_films_chart->options['height'], 300);
        $this->assertGreaterThan(0, $grossing_films_chart->options['background']['R']);
        $this->assertGreaterThan(0, $grossing_films_chart->options['background']['G']);
        $this->assertGreaterThan(0, $grossing_films_chart->options['background']['B']);
      }

    }

    public function testSetOptionsAll()
    {
      $grossing_films_chart = new GrossingFilmsChart($this->GROSSING_FILMS, [
          'show_best_fit' => true,
                  'width' => 400,
                 'height' => 20,
             'background' => array(20, 30, 40),
      ]);

      $this->assertEquals($grossing_films_chart->options['show_best_fit'], true);
      $this->assertEquals($grossing_films_chart->options['width'], 400);
      $this->assertEquals($grossing_films_chart->options['height'], 300);
      $this->assertEquals($grossing_films_chart->options['background']['R'], 20);
      $this->assertEquals($grossing_films_chart->options['background']['G'], 30);
      $this->assertEquals($grossing_films_chart->options['background']['B'], 40);
    }

    public function testCreationOfImage()
    {
      if(file_exists(OUTPUT_IMAGE)) unlink(OUTPUT_IMAGE);

      $grossing_films_chart = new GrossingFilmsChart($this->GROSSING_FILMS);
      $this->assertFileNotExists(OUTPUT_IMAGE);

      $grossing_films_chart->draw();

      $this->assertFileExists(OUTPUT_IMAGE);
    }

  }
