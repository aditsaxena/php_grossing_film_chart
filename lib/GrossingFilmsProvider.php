<?php
  /**
  * XML Extractor
  * It's designed to fetch the Grossing Films Table from Wikipedia's page,
  * parse and return an array of hashes of the useful data (eg. Title, Gross, Year).
  *
  * eg.
  *   # Extract the xml from a HTML string
  *   $grossing_films_class = new GrossingFilmsProvider("<html><head><title>Wikipedia's Grossing Films</title>...");
  *   $grossing_films = $grossing_films_class->extract(); # Array of hashes
  *
  *   # Extract the xml from a File / URI
  *   $grossing_films_class = new GrossingFilmsProvider();
  *   $grossing_films_class->loadFromURI("http://en.wikipedia.org/wiki/List_of_highest-grossing_films");
  *   $grossing_films = $grossing_films_class->extract(); # Array of hashes
  *
  */
  class XmlNotLoaded extends Exception {}

  class GrossingFilmsProvider
  {
    
    function __construct($xml_string = null)
    {
      $this->xml = simplexml_load_string($xml_string);
    }

    public function loadFromURI($url)
    {
      $this->xml = simplexml_load_file($url);
    }

    public function extract()
    {
      if($this->xml == null) throw new XmlNotLoaded("Please load XML first", 1);
      
      $grossing_films = array();

      $start_from_row = 2; # Row one is the header of the table)
      
      foreach ($this->xml->xpath('//*[@id="mw-content-text"]/table[1]/tr') as $inode => $node) {
        if ($inode >= $start_from_row - 1) {
          $xml_raw_data = $node->td;
          $grossing_films[] = array(
            'title' => (String)$node->xpath('th/i/a')[0],
            'gross' => $this->getGross($xml_raw_data),
            'year' => (String)$xml_raw_data[2],
          );
        }
      }
      
      return $grossing_films;
    }

    private function getGross($node)
    {
      $gross = (String) $node[1];
      $gross = (Integer) str_replace(array('$', ','), '', $gross);
      return $gross;
    }

  }
