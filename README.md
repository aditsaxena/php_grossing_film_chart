# PHP Code Test

Use PHP throughout and whatever libraries you deem appropriate. Please **do all visualisation heavy lifting in PHP**, so no use of Javascript for graphing for example, though do feel free to use CSS in your final output.

Write a **PHP application to function as a standalone webpage**, e.g. `http://localhost/test.php`, accessible from a web browser where accessing the page performs both of the following actions.

It should work in a normal Apache/Nginx PHP environment using `PHP 5.3` or `5.4`.

Scrape the table on this page for the top 100 grossing films:  
- http://en.wikipedia.org/wiki/List_of_highest-grossing_films


The script should download the page and extract the relevant data.  

Using a graphing library (e.g. GraPHPite, pChart or JpGraph etc.), create a `scatter plot` of the data showing Adjusted Gross Revenue vs Film Year.


## INSTALL

Just open the test file with it's proper url, eg.
    
    http://localhost/grossing-films-chart/test.php


## TEST

You can launch the tests from phpunit

    $ phpunit tests
