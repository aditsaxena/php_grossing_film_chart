<?php

  include 'lib/general_methods.php';

  include 'lib/pChart2.1.4/class/pData.class.php';
  include 'lib/pChart2.1.4/class/pDraw.class.php';
  include 'lib/pChart2.1.4/class/pImage.class.php';
  include 'lib/pChart2.1.4/class/pScatter.class.php';

  /**
  * GrossingFilmsChart
  */
  class GrossingFilmsChart
  {
    
    function __construct($grossing_films, $options = [])
    {
      $this->grossing_films = $grossing_films;
      $this->setOptions($options);
    }

    private function setOptions($options = [])
    {
      $min_width = 300;
      $min_height = 300;
      
      $options['show_best_fit'] = isset($options['show_best_fit']) ? (Boolean) $options['show_best_fit'] : false;

      $options['width'] = isset($options['width']) ? (Integer) $options['width'] : $min_width;
      $options['height'] = isset($options['height']) ? (Integer) $options['height'] : $min_height;
      $options['width'] = max($options['width'], $min_width);
      $options['height'] = max($options['height'], $min_height);

      $options['paddings'] = isset($options['paddings']) ? $options['paddings'] : [20, 20, 50, 100]; # Follows CSS padding convention (top right bottom left)  

      $options['background'] = isset($options['background']) ? $options['background'] : [192, 222, 111];
      $options['background'] = array(
        "R" => (Integer) $options['background'][0],
        "G" => (Integer) $options['background'][1],
        "B" => (Integer) $options['background'][2],
      );
      
      $options['font'] = array('FontName' => 'lib/pChart2.1.4/fonts/Verdana.ttf', 'FontSize' => 8);

      // print_r($options['width']); exit;

      $this->options = $options;
    }

    public function draw()
    {
      $this->pData = new pData();

      $this->insertPoints();
      $this->setAxis();
      $this->setImage();
      $this->setSetScatter();

      $this->myPicture->autoOutput();
    }

    private function setAxis()
    {
      $this->pData->setSerieOnAxis('Year', 0);
      $this->pData->setAxisName(0, 'Year');
      $this->pData->setAxisXY(0, AXIS_X);
      $this->pData->setAxisPosition(0, AXIS_POSITION_BOTTOM);

      $this->pData->setSerieOnAxis('Grossing', 1);
      $this->pData->setAxisName(1, 'Grossing');
      $this->pData->setAxisXY(1, AXIS_Y);
      $this->pData->setAxisPosition(1, AXIS_POSITION_LEFT);
    }

    private function insertPoints()
    {
      // $names = array_pluck('names', $this->grossing_films);
      $grossings = array_pluck('gross', $this->grossing_films);
      $years = array_pluck('year', $this->grossing_films);

      $this->pData->addPoints($years, 'Year');
      $this->pData->addPoints($grossings, 'Grossing');
    }

    private function setImage()
    {
      $paddings = $this->options['paddings'];
      // B8D361
      $this->myPicture = new pImage($this->options['width'], $this->options['height'], $this->pData);
      $this->myPicture->setFontProperties($this->options['font']);
      $this->myPicture->setGraphArea($paddings[3], $paddings[0], $this->options['width'] - $paddings[1], $this->options['height'] - $paddings[2]);

 $this->myPicture->drawFilledRectangle(0,0,$this->options['width'], $this->options['height'], $this->options['background']);

    }

    private function setSetScatter()
    {
      $this->pData->setScatterSerie('Year', 'Grossing');
      $this->pData->setScatterSerieDescription(0, 'This year');
      $this->pData->setScatterSerieColor(0, array('R' => 78, 'G' => 117, 'B' => 142));

      $myScatter = new pScatter($this->myPicture, $this->pData);
      $myScatter->drawScatterScale();
      $myScatter->drawScatterPlotChart();
      if ($this->options['show_best_fit'] === true) {
        $myScatter->drawScatterBestFit();
      }
    }
  }

